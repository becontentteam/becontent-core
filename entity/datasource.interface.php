<?php 
namespace becontent\core\entity;
interface DataSource{
		
	public function link();
	
	public function getField($fieldName);
	
	public function getFieldValue($fieldName);
	
	public function setFieldValue($fieldName, $fieldValue);
	
	public function getKeyField();
	
	public function getKeyFieldValue();

}
?>