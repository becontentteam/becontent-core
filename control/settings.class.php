<?php

namespace becontent\core\control;

class Settings {
	/**
	 * This path is used in case of resource not found issue.
	 * All standard resources are included in this "app"
	 * 
	 * @var unknown
	 */
	private static $fallbackAppRoot = "contents/becontent/system";
	
	/**
	 * The name of the selected theme for the website, if not specified it'll be used this one
	 * 
	 * @var unknown
	 */
	private static $template_name = "system";
	
	/**
	 * The path of the folder where config files will be stored and retrieved
	 * 
	 * @var unknown
	 */
	private static $config_path = "contents";
	/**
	 * The path of the folder where contents (i.e.
	 * plugins) will be stored and retrieved
	 * 
	 * @var unknown
	 */
	private static $contents_path = "contents";
	/**
	 * The path of the folder where skins will be stored and retrieved
	 * 
	 * @var unknown
	 */
	private static $skins_path = "skins";
	/**
	 * The path of the folder where compiled templates will be stored and retrieved
	 * 
	 * @var unknown
	 */
	private static $compiled_skins_path = "contents/smarty/compiled";
	/**
	 * The default error route in case of errors
	 * 
	 * @var unknown
	 */
	private static $default_error_route = "/error.php";
	/**
	 * If the modMode is true the system is authorized to verify the database structure and to modify it in case of model mods
	 * 
	 * @var unknown
	 */
	private static $modmode = false;
	
	/**
	 * Error filtering disabled in "debug" mode
	 * 
	 * @var unknown
	 */
	private static $operative_mode = "release";
	
	/**
	 * The path of the root of the actual application executed by the script
	 * it is configured dinamically by the router or by specific classes
	 * it is used by skin and skinlet in order to retrieve scripts etc
	 * 
	 * @var unknown
	 */
	private static $actualAppRoot = "";
	public static function getFallbackAppRoot() {
		return self::$fallbackAppRoot;
	}
	public static function getFallbackSkinsPath() {
		return self::$fallbackAppRoot . '/skins/system/dtml/' . Config::getInstance ()->getConfigurations ()["currentlanguage"];
	}
	public static function getDefaultErrorRoute() {
		return self::$default_error_route;
	}
	public static function getOperativeMode() {
		return self::$operative_mode;
	}
	public static function setOperativeMode($mode) {
		self::$operative_mode = $mode;
		if (Settings::getOperativeMode () == "release")
			error_reporting ( E_ERROR );
		else
			error_reporting ( E_ALL );
	}
	public static function getConfigPath() {
		return self::$config_path;
	}
	public static function getContentsPath() {
		return self::$config_path;
	}
	public static function getActualAppRoot() {
		$turnback = preg_replace ( "/(\/+)/", "\/", $_SERVER ['DOCUMENT_ROOT'] );
		$turnback = preg_replace ( "/" . $turnback . "\/" . "/", "", self::$actualAppRoot );
		return $turnback;
	}
	public static function getSkin() {
		$turnback = preg_replace ( "/(\/+)/", "\/", $_SERVER ['DOCUMENT_ROOT'] );
		$turnback = preg_replace ( "/" . $turnback . "\/" . "/", "", self::$skins_path . "/" . self::$template_name . "/dtml/" . Config::getInstance ()->getConfigurations ()["currentlanguage"] );
		return $turnback;
	}
	public static function getCompiledSkins() {
		return self::$compiled_skins_path;
	}
	public static function setSkinName($template_name) {
		self::$template_name = $template_name;
	}
	public static function getSkinName() {
		return self::$template_name;
	}
	public static function setActualAppRoot($appRoot) {
		$documentRoot = realpath ( $_SERVER ['DOCUMENT_ROOT'] . '\\' );
		
		$realPath = realpath ( $appRoot );
		$appRoot = str_replace ( $documentRoot . '\\', "", $realPath );
		
		self::$actualAppRoot = $appRoot;
	}
	public static function setSkinsPath($skinsPath) {
		$documentRoot = realpath ( $_SERVER ['DOCUMENT_ROOT'] . '\\' );
		
		$realPath = realpath ( $skinsPath );
		
		$skinsPath = str_replace ( $documentRoot . '\\', "", $realPath );
		
		self::$skins_path = $skinsPath;
	}
	public static function getModMode() {
		return self::$modmode;
	}
	public static function setModMode($modMode) {
		self::$modmode = $modMode;
	}
}
?>