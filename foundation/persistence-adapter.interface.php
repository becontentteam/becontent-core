<?php
interface PersistenceAdapter{
	
	public static function getInstance();
	
	public function __construct($parameters);
	
	public function  getDataMediatorByName($name);
	
	public function testConnection($parameters);
	
	public function submitOperation($opKey,$parameters,$inData,$outData);
	
}
?>