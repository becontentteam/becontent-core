<?php 
namespace becontent\core\mediator;
interface DataMediator{
	
	/**
	 * This method will be called in order to register the datamediator to the framework
	 */
	public function connect();
	
	/**
	 * This method will be called to store a new datasource ( in example a new raw in E-R meta_model) for this data-mediator
	 * @param unknown $parameters parameters of saving
	 */
	public function save($parameters);
	
	/**
	 * This method will be called from search engine
	 * @param unknown $parameters
	 * @param unknown $join_parameters
	 * @param unknown $order_parameters
	 * @param unknown $limit_parameters
	 */
	public function search($parameters, $join_parameters, $order_parameters, $limit_parameters);

	/**
	 * 
	 * This method will be called to retrieve from persistence a set of datasources ( i.e. a set of raws in E-R meta_model) and to link them ( i.e. Integrity Constraints linking in E-R meta_model) for this data-mediator
	 * Data sources will be stored internally and will be accessible through @method getDataSources()
	 * @param unknown $parameters
	 * @param unknown $join_parameters
	 * @param unknown $order_parameters
	 * @param unknown $limit_parameters
	 */
	public function retrieveAndLink($parameters, $join_parameters, $order_parameters, $limit_parameters);
	
	/**
	 * This method will be called to retrieve a set of unlinked datasources for this data-mediator
	 * Data sources will be stored internally and will be accessible through @method getDataSources()
	 * @param unknown $parameters
	 * @param unknown $join_parameters
	 * @param unknown $order_parameters
	 * @param unknown $limit_parameters
	 */
	public function retrieveOnly($parameters, $join_parameters, $order_parameters, $limit_parameters);
	
	/**
	 * This method will be called to update an identified DataSource ( i.e. a raw for E-R meta_model)
	 * @param unknown $parameters
	 * @param unknown $new_values
	 */
	public function update($parameters, $new_values);
	
	/**
	 * This method will be called to delete an identified DataSource ( i.e. a raw for E-R meta_model)
	 * @param unknown $parameters
	 */
	public function delete($parameters);
	
	/**
	 * This method will be called to access to DataSources retrieved from @method retrieveAndLink() or @method retrieveOnly() 
	 */
	public function getDataSources();
}
?>